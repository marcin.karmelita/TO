function [W] = data(filename, delimiter)

[names, X, Y] = readdata(filename, delimiter);

n = size(X,1);
X = [ones(n,1), X];

H = X' * X;
G = X' * Y;
m = size(X, 2);

delta = 10e-5;
d = abs(det(H));
if (d < 10e-8)
  H = H + delta * eye(m);
endif

W = inv(H) * G;