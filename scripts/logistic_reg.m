function [w, iter_times] = logistic_reg(X, y)

  [n m] = size(X);
  start = now();
  lambda = 1e-6;
  w = zeros(m, 1);
  epsilon = 1e-6;
  B = 1/2 * eye(n);
  G = lambda * w - X'*B*y;
  
  iter_times = [now()-start];

  while norm(G) > epsilon
    start = now();
    B = 1./(1+exp(y.*(X*w)));
    B = diag(B);
    
    H = (X'*B*(eye(n)-B)*X);
    
    if det(H) < 10e-6
        H = lambda * eye(m) +(X'*B*(eye(n)-B)*X);
        G = lambda * w - X' * B * y;
    else
        G =  lambda * w - X' * B * y;
    end
    
    w = w - inv(H)*G;
    iter_times = [now()-start iter_times];
    end

end

