function [w, errors_log, errors_01] = logistic_sgd(X, y)

  [n m] = size(X);
  start = now();
  rp = randperm(n);
  y = y(rp);
  X = X(rp,:);
  w = zeros(m, 1);
  errors_log = [];
  errors_01 = [];

  %===SGD params======
  epochs = 30; %24;
  steps = 1;
  const = 0.04; %0.93;
  %===================

  for epoch = 0:epochs
    for it = 1:n
      alpha = const/sqrt(steps);
      denominator = 1 + exp(y(it) * w' * X(it,:)');
      G = y(it)/denominator*X(it,:)';
      w = w + alpha * G;
      steps = steps + 1;
    endfor
    errors_log = [errors_log; sum(log(1+exp(-y.*(X*w))))/n];
    errors_01 = [errors_01; (sum(y.*(X*w) < 0))/n];
  endfor
  time = now() - start
end
