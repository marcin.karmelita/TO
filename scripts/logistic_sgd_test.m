function [w, errors_log, errors_01] = logistic_sgd_test(X, y, epochs, const)

  [n m] = size(X);
  rp = randperm(n);
  y = y(rp);
  X = X(rp,:);
  w = zeros(m, 1);
  errors_log = [];
  errors_01 = [];

  %===SGD params======
  steps = 1;
  %===================

  for epoch = 0:epochs
    for it = 1:n
      alpha = const/sqrt(steps);
      denominator = 1 + exp(y(it) * w' * X(it,:)');
      G = y(it)/denominator*X(it,:)';
      w = w + alpha * G;
      steps = steps + 1;
    endfor
    errors_log = [errors_log; sum(log(1+exp(-y.*(X*w))))];
    errors_01 = [errors_01; (sum(y.*(X*w) < 0) / n)];
  endfor
end
