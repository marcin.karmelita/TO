function [names, X, Y] = preparedata(filename, delimiter)

[names, X, Y] = readdata(filename, delimiter);

n = size(X,1);
X = [ones(n,1), X];

