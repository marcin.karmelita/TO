% najperw zmień katalog na ten, w którym zapisane są pliki funkcją:
% chdir folder

function [names, X, Y] = readdata(filename, delimiter)
% Funkcja odczytuje zbiór danych z pliku.
% Pierwsza linijka powinna być nagłówkowa z nazwami zmiennych.
% Każda kolejna linijka to wektor x i wartość y.
% Wszystkie elementy oddzielone za pomocą delimiter (np spacja).
% Funkcja zwraca komórkę z nazwami, macierz X (każda obserwacja to
% wiersz tej macierzy) oraz wektor kolumnowy Y.
    fileID = fopen(filename, 'r');
    header = fgetl(fileID);
    names = strread(header,'%s','delimiter', delimiter);
    X = dlmread(filename,delimiter,1,0);
    
    Y = X(:,end);
    X = X(:,1:end-1);
end
