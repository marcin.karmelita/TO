function [mse] = sgd()

[names, X, y] = readdata("DenBosch.data", " ");
[n m] = size(X);
w = zeros(m, 1);
c= 0.1;
rp = randperm(n);
y = y(rp);
X = X(rp,:);

Xst = (X - repmat(mean(X), rows(X), 1)) ./ repmat(std(X), rows(X), 1);
yst = (y - repmat(mean(y), rows(y), 1)) ./ repmat(std(y), rows(y), 1);

wst = inv(Xst'*Xst)*Xst'*yst;

mse = mean((yst - Xst*wst).^2);

i = 1
for epoch = 0:50
  for it = 1:n
    xit = Xst(it,:)';
    yit = yst(it);
    G = (yit - w'*xit)*xit;
    alpha = c/sqrt(i);
    w = w + 2*alpha*G;
    i+=1;
  end
  L = mean((yst - Xst*w).^2)
   
end

end