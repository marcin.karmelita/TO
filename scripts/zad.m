function [] = zad()

[w_nr, nr_error_log, nr_error_01] = zad1;
[w_sgd, sgd_errors_log, sgd_errors_01] = zad2;
zad3(nr_error_log, nr_error_01, sgd_errors_log, sgd_errors_01);

euc = norm(w_nr-w_sgd)

dlmwrite("w_nr.txt", w_nr);
dlmwrite("w_sgd.txt", w_sgd);
end