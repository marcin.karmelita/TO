function [w, errors_log, errors_01] = zad2()

%[names, X_test, y_test] = preparedata("small_file12_test.txt", " ");
%[names, X_train, y_train] = preparedata("small_file12_train.txt", " ");
[names, X_test, y_test] = preparedata("file12_test.txt", " ");
[names, X_train, y_train] = preparedata("file12_train.txt", " ");

%X_test = [ones(size(X_test, 1), 1) X_test];
%X_train = [ones(size(X_train, 1), 1) X_train];

[w, errors_log, errors_01] = logistic_sgd(X_train, y_train);

% convergence log loss
figure(1);
plot(errors_log);
%bar(errors_log);
title ("Zbieżność algorytmu SGD - błąd logistyczny");
xlabel ("Epoka");
ylabel ("Błąd logistyczny");

% convergence 01 loss
figure(2);
plot(errors_01);
%bar(errors_01);
title ("Zbieżność algorytmu SGD - błąd 0/1");
xlabel ("Epoka");
ylabel ("Błąd 0/1");

% train loss
L_log_train = L_log(y_train, X_train, w)
L_01_train = L_01(y_train, X_train, w)

% test loss
L_log_test = L_log(y_test, X_test, w)
L_01_test = L_01(y_test, X_test, w)

% hist
figure(3);
hist(w)
title ("Rozkład wag");
xlabel ("Liczebność");
ylabel ("Wartości wag");

end