function [] = zad2_tests()



%[names, X_test, y_test] = preparedata("small_file12_test.txt", " ");
%[names, X_train, y_train] = preparedata("small_file12_train.txt", " ");
[names, X_test, y_test] = preparedata("file12_test.txt", " ");
[names, X_train, y_train] = preparedata("file12_train.txt", " ");


%alphas = [0.095, 0.094 ,0.093, 0.092, 0.091];

epochs = 50;
alpha = 0.1;
for j = 1:3
  [w, errors_log, errors_01] = logistic_sgd_test(X_train, y_train, epochs, alpha);
  alpha = alpha - 0.03
  % test loss
  L_log_test = L_log(y_test, X_test, w)
endfor
epochs = epochs + 10;

end